<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // Add new columns
            $table->softDeletes();
            $table->boolean('active')->default(true)
                ->comment('Allow values: 0, 1. Notes: 0: false, 1: true. Default = 1.');
            $table->boolean('is_verified')->default(false)
                ->comment('Verify status. Allow values: 0, 1. Notes: 0: false, 1: true. Default = 0.');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('default_password')->nullable();
            $table->string('api_token', 60)->nullable()->unique();
            $table->date('birthday')->nullable();
            $table->tinyInteger('gender')->nullable()
                ->comment('Allow values: 0: none, 1: male, 2: female, 3: others.');
            $table->text('address')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('fb_id')->nullable();
            $table->text('fb_token')->nullable();
            $table->string('gg_id')->nullable();
            $table->text('gg_token')->nullable();
            $table->string('tw_id')->nullable();
            $table->text('tw_token')->nullable();
            $table->dateTime('last_login')->nullable()
                ->comment('Last login datetime');
            $table->string('locale', 10)->nullable()
                ->comment('User locale.');
            $table->string('timezone')->nullable()
                ->comment('Timezone key strings (from PHP timezone list function).');
            $table->string('email_tmp')->nullable()->after('email')
                ->comment('For change email function.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropColumn(['active', 'is_verified', 'first_name', 'last_name', 'default_password', 'birthday', 'gender',
                'address', 'phone', 'fb_id', 'fb_token', 'gg_id', 'gg_token', 'tw_id', 'tw_token', 'last_login', 'locale',
                'timezone', 'email_tmp',]);
        });
    }
}
