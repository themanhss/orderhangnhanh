<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = Carbon::now();

        DB::table('roles')->insert([
            [
                'id' => 1,
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'active' => 1,
                'name' => 'Administrator',
                'description' => 'This is admin user',
                'permission' => '{}',
            ],
            [
                'id' => 2,
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
                'active' => 0,
                'name' => 'User',
                'description' => 'This is normal user',
                'permission' => '{}',
            ],

        ]);
    }
}
