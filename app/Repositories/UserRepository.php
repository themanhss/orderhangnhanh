<?php

namespace App\Repositories;

use App\Role;
use App\User;
use App\Helpers\Helper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends AppBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'photo',
        'description',
        'is_admin',
        'remember_token',
        'created_at',
        'updated_at',
        'role_id',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * @param Request $request
     * @param int $userType User type. 0: User, 1: Admin.
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getAllUsers(Request $request, $userType = 0)
    {
        /** @var \App\User $me */
        $me = Auth::guard('admin')->user();

        /** @var \App\User $userInstance */
        $userInstance = User::getInstance();

        $this->scopeQuery(function (\Illuminate\Database\Eloquent\Model $model) use ($request, $userInstance, $me, $userType) {
            // Relation
            $model = $model->with(['role']);
            // Order
            $model = $model->orderBy('users.' . $userInstance->getKeyName(), User::ORDER_DESC);

            // Filter by type (is admin or not)
            if (in_array($userType, [User::USER_TYPE_ADMIN, User::USER_TYPE_NORMAL])) {
                $model = $model->where('users.is_admin', ($userType == User::USER_TYPE_ADMIN) ? User::IS_YES : User::IS_NO);
            }

            // Except me
            $model = $model->whereNotIn('users.' . $userInstance->getKeyName(), [$me->id, User::SUPER_ADMIN_ID]);

            // Search by keyword
            if ($request->has('keyword') && $request->get('keyword') != '') {
                $keyword = $request->get('keyword');

                $model = $model->where(function ($query) use ($keyword) {
                    $query->where('users.name', 'LIKE', '%' . $keyword . '%');
                    $query->orWhere('users.email', 'LIKE', '%' . $keyword . '%');
                });
            }

            // Filter by role
            if ($request->has('role_id') && $request->get('role_id') != '') {
                $model = $model->where('users.role_id', (int)$request->get('role_id'));
            }

            // Filter by active status
            if ($request->has('active') && $request->get('active') != '') {
                $model = $model->where('users.active', (int)$request->get('active'));
            }

            // Filter by verify status
            if ($request->has('is_verified') && $request->get('is_verified') != '') {
                $model = $model->where('users.is_verified', (int)$request->get('is_verified'));
            }

            return $model;
        });

        $this->pushCriteria(new RequestCriteria($request));
        $users = $this->paginate();

        return $users;
    }

    /**
     * Get role list
     *
     * @param Request $request
     * @param int $userType User type. 0: User, 1: Admin.
     * @return \Illuminate\Support\Collection
     */
    public function getRoleList(Request $request, $userType = 0)
    {
        /** @var \App\Role $roleInstance */
        $roleInstance = Role::getInstance();
        $roles = Role::where('roles.active', Role::IS_YES);

        // With User List, Role = User
        if ($userType == User::USER_TYPE_ADMIN) {
            // Other admin roles
            $roles->whereNotIn('roles.' . $roleInstance->getKeyName(), $roleInstance->getNormalUserRoles());
        } else if ($userType == User::USER_TYPE_NORMAL) {
            // User
            $roles->whereIn('roles.' . $roleInstance->getKeyName(), $roleInstance->getNormalUserRoles());
        }

        // Get list
        /** @var \Illuminate\Support\Collection $roles */
        $roles = $roles->pluck('roles.name', 'roles.' . $roleInstance->getKeyName());
        // Translate
        $roles->transform(function ($item, $key) {
            return Helper::trans($item, [], 'role');
        });

        return $roles;
    }

    /**
     * Overwrite create function from base
     *
     * @param array $attributes
     * @return \App\User
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function create(array $attributes)
    {
        // Begin transaction
        DB::beginTransaction();

        // Random password
        $password = User::randomPassword();
        $attributes['default_password'] = $password;
        $attributes['password'] = bcrypt($password);

        // Create new user
        $user = parent::create($attributes);

        // Commit transaction
        DB::commit();

        // Send mail
        dispatch(new \App\Jobs\SendAdminPasswordEmail($user));

        return $user;
    }

}
