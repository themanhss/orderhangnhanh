<?php

namespace App;

/**
 * Class BannerTranslation
 * @package App\Entities
 * @version October 17, 2018, 7:29 am UTC
 */
class BannerTranslation extends AppModel
{
    public $timestamps = false;

    protected $fillable = [
        'title_1',
        'title_2',
        'button_1',
        'button_2',
        'link_1',
        'link_2',
        'description',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'id' => 'integer',
        'banner_id' => 'integer',
        'locale' => 'string',
        'title_1' => 'string',
        'title_2' => 'string',
        'button_1' => 'string',
        'button_2' => 'string',
        'link_1' => 'string',
        'link_2' => 'string',
        'description' => 'string'
    ];*/

}
