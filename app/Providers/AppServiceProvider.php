<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Blade;
use URL;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultStringLength(191);

        Blade::directive('pushonce', function ($expression) {
            $isDisplayed = '__pushonce_' . trim(substr($expression, 1, -1));
            return "<?php if(!isset(\$__env->{$isDisplayed})): \$__env->{$isDisplayed} = true; \$__env->startPush({$expression}); ?>";
        });

        Blade::directive('endpushonce', function ($expression) {
            return '<?php $__env->stopPush(); endif; ?>';
        });

        View::share('appTitle', "");

        /**
         * @link https://stackoverflow.com/questions/29549660/get-laravel-5-controller-name-in-view#answer-29549985
         */
        app('view')->composer('layouts.admin', function ($view) {
            $prefix = app('request')->route()->getPrefix();
            $action = app('request')->route()->getAction();
            $controller = class_basename($action['controller']);
            list($controller, $action) = explode('@', $controller);
            $params = app('request')->route()->parameters();
            $baseUrl = URL::to('/') . '/';

            $view->with(compact('prefix', 'controller', 'action', 'params', 'baseUrl'));
            /*View::share('prefix', $prefix);
            View::share('controller', $controller);
            View::share('action', $action);
            View::share('params', $params);
            View::share('baseUrl', $baseUrl);*/
        });

        /*\DB::listen(function ($query) {
            $arrOutput = [
                $query->sql,
                $query->bindings,
                // $query->time
            ];
            // var_dump($arrOutput);
            file_put_contents(base_path() .  '/storage/logs/dump_db_queries.txt', var_export($arrOutput, true)
                . PHP_EOL . '--------------------------------------------------------' . PHP_EOL, FILE_APPEND);
        });*/

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
