<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    private $except = [
        'App\Modules\ContentManager\Controllers\MediaController@index',
        'App\Modules\ContentManager\Controllers\MediaController@store',
        'App\Modules\ContentManager\Controllers\MediaController@images',
        'App\Modules\ContentManager\Controllers\MediaController@destroy',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        // User is guest
        if (\Auth::guard($guard)->guest()) {
            return redirect('/');
        }

        /** @var \App\User $user */
        $user = \Auth::guard($guard)->user();
        $roleId = $user->role_id;

        // When is not admin user
        if (!$user->isAdmin()) {
            return redirect('/');
        }

        // User is super admin
        if ($user->isSuperAdmin()) {
            return $next($request);
        }

        // Get role from cache if exist
        $roleCacheName = "role_permission_{$roleId}.php";
        $roleCacheFile = storage_path('app') . DIRECTORY_SEPARATOR . $roleCacheName;

        if (empty($user->roles) && file_exists($roleCacheFile)) {
            include_once $roleCacheFile;
            // $role_permission get from include file

            if (!empty($role_permission)) {
                $user->setRelation('roles', new \App\Role($role_permission));
            }
        }

        // Check permission by user role (permission)
        $role = $user->roles;

        if (!empty($role)) {
            if (!$role->active) {
                return redirect('/');
            }
        }
        // Check user has access
        if ($this->userHasAccessTo($request) || in_array(\Route::currentRouteAction(), $this->except)) {
            return $next($request);
        }

        // Request ajax
        if ($request->ajax()) {
            return response()->json(['status' => 403, 'success' => false, 'message' => 'Unauthorised.'], 403);
        }
        return abort(403);
    }

    /*
    |--------------------------------------------------------------------------
    | Additional helper methods for the handle method
    |--------------------------------------------------------------------------
    */
    /**
     * Checks if user has access to this requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @param string $guard
     * @return Boolean true if has permission otherwise false
     */
    protected function userHasAccessTo($request, $guard = 'admin')
    {
        return $this->hasPermission($request, $guard);
    }

    /**
     * hasPermission Check if user has requested route permission
     *
     * @param  \Illuminate\Http\Request $request
     * @param string $guard
     * @return Boolean true if has permission otherwise false
     */
    protected function hasPermission($request, $guard = 'admin')
    {
        $required = $this->requiredPermission($request, $guard);

        return !$this->forbiddenRoute($request) && \Auth::guard($guard)->user()->hasPermission($required);
    }

    /**
     * Extract required permission from requested route
     *
     * @param  \Illuminate\Http\Request $request
     * @param string $guard
     * @return array permission_slug connected to the Route
     */
    protected function requiredPermission($request, $guard = 'admin')
    {
        $action = $request->route()->getAction();
        $required = [];

        if (isset($action['controller'])) {
            $controller = isset($action['namespace']) ? explode("{$action['namespace']}\\", $action['controller']) : [];

            $required = !empty($controller) ? (array)$controller[1] : (array)$action['controller'];
        }

        return $required;
    }

    /**
     * Check if current route is hidden to current user role
     *
     * @param  \Illuminate\Http\Request $request
     * @param string $guard
     * @return Boolean true/false
     */
    protected function forbiddenRoute($request, $guard = 'admin')
    {
        $action = $request->route()->getAction();

        if (isset($action['except'])) {
            return $action['except'] == \Auth::guard($guard)->user()->role->id;
        }

        return false;
    }
}
