<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UpdateUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Get data from route request
        /** @var \App\User $user */
        $user = $this->route('user');

        $rules = array_merge(User::$rules, [
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
            /*'role_id' => 'required',*/
        ]);

        return $rules;
    }
}
