<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class CreateUserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(User::$rules, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            /*'role_id' => 'required',*/
        ]);

        return $rules;
    }
}
