<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

// Redirect by default from Laravel core
Route::get('/home', function () {
    return redirect('/');
});

// Test commands
Route::get('/test_commands/{name}', 'HomeController@test_commands');

// Change language
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::auth();
// Custom auth routes
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Change mail
Route::get('/confirm-change-email/{token}', ['as' => 'users.confirmChangeEmail', 'uses' => 'UserController@confirmChangeEmail']);
Route::get('/changed-email-success', ['as' => 'users.changedEmailSuccess', 'uses' => 'UserController@changedEmailSuccess']);