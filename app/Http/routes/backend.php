<?php

/*
  |--------------------------------------------------------------------------
  | Backend Routes
  |--------------------------------------------------------------------------
  |
  | Here is where all Backend routes are defined.
  |
 */

// Change password
Route::get('password/change-password', ['as' => 'admin.password.changePasswordForm', 'uses' => 'Auth\ChangePasswordController@changePasswordForm']);
Route::post('password/change-password', ['as' => 'admin.password.changePassword', 'uses' => 'Auth\ChangePasswordController@changePassword']);
// Users
Route::put('users/item-actions', ['as' => 'admin.users.itemActions', 'uses' => 'UserController@itemActions']);
Route::resource('users', 'UserController', ['as' => 'admin']);
// Countries
Route::resource('countries', 'CountryController', ['as' => 'admin']);
// Contacts
Route::resource('contacts', 'ContactController', ['as' => 'admin']);
// Roles
Route::resource('roles', 'RoleController', ['as' => 'admin']);
// Banners
Route::put('banners/change-order', ['as' => 'admin.banners.changeOrder', 'uses' => 'BannerController@changeOrder']);
Route::resource('banners', 'BannerController', ['as' => 'admin']);
