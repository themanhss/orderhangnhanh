<?php

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where all API routes are defined.
  |
 */


/*
|--------------------------------------------------------------------------
| API routes of Country
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api/v1', 'middleware' => ['api', 'jwt.auth']], function () {
    Route::get('countries', ['as' => 'api.countries.index', 'uses' => 'CountryAPIController@index']);
    Route::post('countries', ['as' => 'api.countries.index', 'uses' => 'CountryAPIController@store']);
    Route::get('countries/{countries}', ['as' => 'api.countries.show', 'uses' => 'CountryAPIController@show']);
    Route::put('countries/{countries}', ['as' => 'api.countries.update', 'uses' => 'CountryAPIController@update']);
    Route::patch('countries/{countries}', ['as' => 'api.countries.patch', 'uses' => 'CountryAPIController@update']);
    Route::delete('countries{countries}', ['as' => 'api.countries.destroy', 'uses' => 'CountryAPIController@destroy']);
});


/*
|--------------------------------------------------------------------------
| API routes of Contact
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api/v1', 'middleware' => ['api', 'jwt.auth']], function () {
    Route::get('contacts', ['as' => 'api.contacts.index', 'uses' => 'ContactAPIController@index']);
    Route::post('contacts', ['as' => 'api.contacts.index', 'uses' => 'ContactAPIController@store']);
    Route::get('contacts/{contacts}', ['as' => 'api.contacts.show', 'uses' => 'ContactAPIController@show']);
    Route::put('contacts/{contacts}', ['as' => 'api.contacts.update', 'uses' => 'ContactAPIController@update']);
    Route::patch('contacts/{contacts}', ['as' => 'api.contacts.patch', 'uses' => 'ContactAPIController@update']);
    Route::delete('contacts{contacts}', ['as' => 'api.contacts.destroy', 'uses' => 'ContactAPIController@destroy']);
});


/*
|--------------------------------------------------------------------------
| API routes of Role
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api/v1', 'middleware' => ['api', 'jwt.auth']], function () {
    Route::get('roles', ['as' => 'api.roles.index', 'uses' => 'RoleAPIController@index']);
    Route::post('roles', ['as' => 'api.roles.index', 'uses' => 'RoleAPIController@store']);
    Route::get('roles/{roles}', ['as' => 'api.roles.show', 'uses' => 'RoleAPIController@show']);
    Route::put('roles/{roles}', ['as' => 'api.roles.update', 'uses' => 'RoleAPIController@update']);
    Route::patch('roles/{roles}', ['as' => 'api.roles.patch', 'uses' => 'RoleAPIController@update']);
    Route::delete('roles{roles}', ['as' => 'api.roles.destroy', 'uses' => 'RoleAPIController@destroy']);
});


/*
|--------------------------------------------------------------------------
| API routes of Banner
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api/v1', 'middleware' => ['api', 'jwt.auth']], function () {
    Route::get('banners', ['as' => 'api.banners.index', 'uses' => 'BannerAPIController@index']);
    Route::post('banners', ['as' => 'api.banners.index', 'uses' => 'BannerAPIController@store']);
    Route::get('banners/{banners}', ['as' => 'api.banners.show', 'uses' => 'BannerAPIController@show']);
    Route::put('banners/{banners}', ['as' => 'api.banners.update', 'uses' => 'BannerAPIController@update']);
    Route::patch('banners/{banners}', ['as' => 'api.banners.patch', 'uses' => 'BannerAPIController@update']);
    Route::delete('banners{banners}', ['as' => 'api.banners.destroy', 'uses' => 'BannerAPIController@destroy']);
});
