<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Lang;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Frontend
 */
class UserController extends Controller
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();

        $this->userRepository = $userRepo;
    }

    /**
     * Update the specified User in storage.
     *
     * @param Request $request
     *
     * @param string $token
     * @return Response
     */
    public function confirmChangeEmail(Request $request, $token)
    {
        // Decode object
        $data = json_decode(base64_decode($token), true);

        if (empty($data['id'])) {
            Flash::error(Lang::get('messages.user.not_found'));
        }

        /** @var \App\User $user */
        $user = $this->userRepository->findWithoutFail($data['id']);

        if (empty($user)) {
            Flash::error(Lang::get('messages.user.not_found'));
        }

        $user->email = $user->email_tmp;
        $user->email_tmp = null;
        $user->save();

        Flash::success(Lang::get('messages.user.changed_email_successfully'));

        return redirect(url('/changed-email-success'));
    }

    /**
     * Display the specified User.
     *
     * @return Response
     */
    public function changedEmailSuccess()
    {
        return view('auth.emails.changed_email_success');
    }

}
