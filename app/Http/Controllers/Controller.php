<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        //parent::__construct();

        // Enabled session variable for KCFINDER
        if (!session_id()) {
            session_start();
        }

        // Overwrite KCFinder config
        $configKCFinder = config('kcfinder');

        if (!empty($configKCFinder)) {
            if (isset($configKCFinder['uploadURL'])) {
                $configKCFinder['uploadURL'] = url($configKCFinder['uploadURL']);
            }

            // Get from laravel helper
            if (isset($configKCFinder['uploadDir'])) {
                $configKCFinder['uploadDir'] = public_path($configKCFinder['uploadDir']);
            }

            $_SESSION['KCFINDER'] = $configKCFinder;
        }

    }

    /**
     * @param array $errors
     * @return string|null
     */
    public function getFirstError($errors = null) {
        if ($errors == null || empty($errors) || !is_array($errors) || count($errors) == 0)
            return null;

        /**
         * @var string $field
         * @var array $errors - Array<String>
         */
        foreach ($errors as $field => $messages) {
            return $errors[$field];
        }

        return null;
    }

    /**
     * @param mixed|null $errors
     * @return string|null
     */
    public function getFirstErrorMessage($errors = null) {
        if ($errors !== null) {
            /**
             * @var string $field
             * @var array $errors - Array<String>
             */
            foreach ($errors as $field => $messages) {
                if ($messages !== null && count($messages) > 0)
                    return $messages[0];
            }
        }

        return null;
    }

    /**
     * @param Request $request
     * @param array $inputs
     * @return Request
     */
    public function rejectInputRequest($request, $inputs) {
        foreach ($inputs as $input) {
            if ($request->has($input)) {
                $request->request->remove($input);
            }
        }

        return $request;
    }

}
