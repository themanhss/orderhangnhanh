<?php

namespace App\Http\Controllers\Backend;

use App\Role;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Flash;
use Lang;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoleController
 * @package App\Http\Controllers\Backend
 */
class RoleController extends BackendController
{
    /** @var RoleRepository $roleRepository */
    private $roleRepository;

    /**
     * RoleController constructor.
     * @param RoleRepository $roleRepo
     */
    public function __construct(RoleRepository $roleRepo)
    {
        parent::__construct();

        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        /** @var \App\Role $roleInstance */
        $roleInstance = Role::getInstance();
        $this->roleRepository->scopeQuery(function (\Illuminate\Database\Eloquent\Model $model) use ($roleInstance) {
            $model = $model->whereNotIn($roleInstance->getKeyName(), $roleInstance->getNormalUserRoles());

            return $model;
        });

        $this->roleRepository->pushCriteria(new RequestCriteria($request));
        $roles = $this->roleRepository->paginate();

        return view('admin.roles.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateRoleRequest $request)
    {
        $input = [
            'active' => Role::IS_YES,
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'permission' => $this->_addMorePermissionPosted($request->get('permission')),
        ];

        $role = $this->roleRepository->create($input);

        Flash::success(Lang::get('messages.role.created_successfully'));

        return redirect(route('admin.roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            Flash::error(Lang::get('messages.role.not_found'));

            return redirect(route('admin.roles.index'));
        }

        return view('admin.roles.edit')->with('role', $role);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            Flash::error(Lang::get('messages.role.not_found'));

            return redirect(route('admin.roles.index'));
        }

        return view('admin.roles.edit')->with('role', $role);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  int $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateRoleRequest $request)
    {
        /** @var \App\Role $role */
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            Flash::error(Lang::get('messages.role.not_found'));

            return redirect(route('admin.roles.index'));
        }

        $input = [
            'active' => $request->get('active', $role->active),
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'permission' => $this->_addMorePermissionPosted($request->get('permission')),
        ];

        $role = $this->roleRepository->update($input, $id);

        Flash::success(Lang::get('messages.role.updated_successfully'));

        return redirect(route('admin.roles.edit', [$id]));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            Flash::error(Lang::get('messages.role.not_found'));

            return redirect(route('admin.roles.index'));
        }

        $this->roleRepository->delete($id);

        Flash::success(Lang::get('messages.role.deleted_successfully'));

        return redirect(route('admin.roles.index'));
    }

    /**
     * Add more permission to permission posted (Example : if permission is create, need to add store)
     *
     * @param array $permission
     * @return array
     */
    protected function _addMorePermissionPosted($permission)
    {
        // Check duplicate and get unique values
        $permission = $this->_checkDuplicatePermissions($permission);

        $permission['dashboard'][] = 'default@index';
        $permission['login'][] = 'login@logout';
        $permission['auth'][] = 'auth@logout';
        /*$arr = [
            'admin@show' => [
                'admin@index'
            ]
        ];*/

        if (!empty($permission)) {
            foreach ($permission as $key => $arrAction) {
                foreach ($arrAction as $action) {
                    $fieldData = explode('@', $action);

                    switch ($fieldData[1]) {
                        case 'create':
                            if (!in_array($fieldData[0] . '@store', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@store';
                            }

                            if (!in_array($fieldData[0] . '@index', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@index';
                            }

                            if (!in_array($fieldData[0] . '@show', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@show';
                            }
                            break;
                        case 'edit':
                            if (!in_array($fieldData[0] . '@update', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@update';
                            }

                            if (!in_array($fieldData[0] . '@index', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@index';
                            }

                            if (!in_array($fieldData[0] . '@show', $permission[$key])) {
                                $permission[$key][] = $fieldData[0] . '@show';
                            }
                            break;
                    }

                    /*if (isset($arr[$action])) {
                        foreach ($arr[$action] as $item) {
                            if (!in_array($item, $permission[$key])) {
                                $permission[$key][] = $item;
                            }
                        }
                    }*/
                }
            }
        }

        return $permission;
    }

    /**
     * Check duplicate and get unique values
     *
     * @param array $allPermission
     * @return array
     */
    protected function _checkDuplicatePermissions($allPermission) {
        if ($allPermission && is_array($allPermission)) {
            foreach ($allPermission as $controller => $actions) {
                if (is_array($actions)) {
                    $allPermission[$controller] = array_values(array_unique($actions));
                }
            }
        }

        return $allPermission;
    }

}
