<?php

namespace App\Http\Controllers\Backend;

use Lang;
use App\User;
use App\Post;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Flash;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Backend
 */
class UserController extends BackendController
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();

        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->getAllUsers($request, User::USER_TYPE_ALL);
        $roles = $this->userRepository->getRoleList($request, User::USER_TYPE_ALL);

        // Status
        $active_statuses = User::active_statuses();
        $verify_statuses = User::verify_statuses();

        return view('admin.users.index', ['model' => $users])
            ->with(compact('roles', 'active_statuses', 'verify_statuses'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $roles = $this->userRepository->getRoleList($request, User::USER_TYPE_ALL);
        $genders = User::genders();

        return view('admin.users.create', ['model' => ''])
            ->with(compact('roles', 'genders'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        Flash::success(Lang::get('messages.user.created_successfully'));

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function show(Request $request, User $user)
    {
        $id = $user->id;
        $post = Post::where('post_author', $id)
            ->where('post_type', 'post')
            ->orderby('id', 'desc')
            ->get();

        return view('admin.users.show', ['model' => $user, 'post' => $post]);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user)
    {
        $roles = $this->userRepository->getRoleList($request, User::USER_TYPE_ALL);
        $genders = User::genders();

        return view('admin.users.edit', ['model' => $user])
            ->with(compact('roles', 'genders'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $id = $user->id;
        $input = $request->all();
        $changeEmail = false;

        if ($request->has('email')) {
            // When change email
            if($input['email'] != $user->email) {
                // Store new mail in temporary
                $input['email_tmp'] = $input['email'];
                $changeEmail = true;
            }

            // Not allow change email now
            unset($input['email']);
        }

        // Save data
        $user = $this->userRepository->update($input, $id);

        if ($changeEmail) {
            // Send mail
            $this->dispatch(new \App\Jobs\SendChangeMailConfirmation($user));
        }

        Flash::success(Lang::get('messages.admin.updated_successfully'));

        return redirect(route('admin.users.show', ['user' => $id]));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function destroy(Request $request, User $user)
    {
        $id = $user->id;
        $this->userRepository->delete($id);

        Flash::success(Lang::get('messages.admin.deleted_successfully'));

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified ActivationCode from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function itemActions(Request $request)
    {
        $input = $request->all();

        if (empty($input['id']) || empty($input['key']) || !isset($input['value'])) {
            return $this->sendError('Invalid information. Please try again.');
        }

        $value = $input['value'];
        $key = $input['key'];
        $id = $input['id'];
        $getModel = $this->userRepository->model();
        $model = new $getModel;
        /** @var \App\User $data */
        $data = $model->find($id);

        if (empty($data)) {
            return $this->sendError('Not found.');
        }

        if ($key == 'status') {
            $data->active = (int)$value;
            $data->save();
        } else if ($key == 'delete') {
            if ($data->id == 1) {
                return $this->sendError("Can't delete.", 500);
            }
        }

        return $this->sendResponse(null, 'Process successfully.');
    }

}
