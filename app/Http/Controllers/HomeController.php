<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Theme;
use App\Modules\ContentManager\Models\Articles;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$blog = Articles::where('post_type','post')->where('post_status','publish')->orderBy('id', 'desc')->paginate(10);
        return view(Theme::frontpage(),['blog'=>$blog]);*/

        return redirect(route('login'));
    }

    /**
     * Calling Commands Via Code:
     * @link https://laravel.com/docs/5.2/artisan#calling-commands-via-code
     *
     * Check if a Laravel Console Command Exists:
     * @link https://stackoverflow.com/a/44154878/10174865
     *
     * @param string $name
     */
    public function test_commands($name)
    {
        // Check command exist
        if (!array_has(\Artisan::all(), $name)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Command not found');
        }

        // Call command by name
        \Artisan::call($name);

        echo 'Success';
        exit();
    }

}
