<?php

namespace App;

use Illuminate\Support\Facades\Storage;

/**
 * Class Role
 * @package App
 * @version October 22, 2018, 4:00 am UTC
 *
 * @property int id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property bool active Active status
 * @property string name
 * @property string description
 * @property string permission
 */
class Role extends AppModel
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;

    public $table = 'roles';
    public $fillable = [
        'created_at',
        'updated_at',
        'active',
        'name',
        'description',
        'permission'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'boolean',
        'name' => 'string',
        'description' => 'string',
        'permission' => 'array'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(\App\User::class);
    }

    /**
     * @param bool $overwrite
     * @return bool
     */
    public function cachePermission($overwrite = true)
    {
        $fileName = "role_permission_{$this->id}.php";

        // Ignore if not allow overwrite with exist file
        if (Storage::disk('local')->exists($fileName) && !$overwrite) {
            return true;
        }

        $varContent = var_export($this->toArray(), true);
        $fileContent = '<?php $role_permission = ' . $varContent . ';';

        return Storage::disk('local')->put($fileName, $fileContent);
    }

    /**
     * @return bool
     */
    public function cleanCachePermission()
    {
        $fileName = "role_permission_{$this->id}.php";

        if (!Storage::disk('local')->exists($fileName)) {
            return true;
        }

        return Storage::disk('local')->delete($fileName);
    }

    /**
     * Fire events when create, update roles
     * The "booting" method of the model.
     * @link https://stackoverflow.com/a/38685534
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // When saved
        static::saved(function ($model) {
            $model->cachePermission();
        });

        static::deleted(function ($model) {
            $model->cleanCachePermission();
        });
    }

    /**
     * Get init user role:
     * 1: Administrator
     * 2: User
     *
     * @return array
     */
    public function getHiddenRoles()
    {
        return [static::ROLE_ADMIN, static::ROLE_USER];
    }

    /**
     * Get normal user roles:
     * 2: User
     *
     * @return array
     */
    public function getNormalUserRoles()
    {
        return [static::ROLE_USER];
    }

    /**
     * Get Admin user role
     *
     * @return array
     */
    public function getAdminRoles()
    {
        return [static::ROLE_ADMIN];
    }

}
