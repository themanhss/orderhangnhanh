@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>User Detail</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="POST" action="#" class="form-view">
                        <input type="hidden" id="data-token" value="{{ csrf_token()}}">
                        <input type="hidden" id="data-link-item-actions" value="{{ Admin::route('users.itemActions') }}">

                        <div class="col-md-4">
                            <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{ $model->photo }}" alt="Avatar" title="Change the avatar">
                        </div>
                        <div class="col-md-8">

                            <!-- Active Field -->
                            <div class="form-group">
                                <div class="@if ($model->active) text-success @else text-danger @endif inline-block">
                                    <span class="glyphicon @if ($model->active) glyphicon glyphicon-record @else glyphicon-ban-circle @endif" aria-hidden="true"></span>
                                    &nbsp;{!! \App\User::active_statuses((int)$model->active) !!}
                                </div>

                                {{-- Ban/un-ban --}}
                                @php($statusVal = ($model->active == \App\User::IS_YES) ? 0 : 1)
                                @php($statusText = ($model->active == \App\User::IS_YES) ? 'ban' : 'unban')
                                @php($statusIcon = ($model->active == \App\User::IS_YES) ? 'fa-ban' : 'fa-play-circle')

                                @if(Helper::checkUserPermission('admin.users.itemActions') && $model->id != Auth::guard('admin')->user()->id)
                                    <a href="javascript:void(0)" class='btn  btn-xs btn-default item_actions' data-link="data-link-item-actions" data-val="{{ $statusVal }}"
                                       data-key="status" data-text="{{ $statusText }}" data-id="{!! $model->id !!}" data-title="{!! $model->name !!}" title="{{ $statusText }}"
                                       style="margin-left: 10px;">
                                        <i class="fa {{ $statusIcon }}" aria-hidden='true'></i>
                                    </a>
                                @endif
                            </div>

                            <!-- Name Field -->
                            <div class="form-group">
                                {!! Html::decode(Form::label('name', trans('strings.user.label_name'))) !!}
                                <div class="form-control">{{ $model->name }}</div>
                            </div>

                            <!-- Email Field -->
                            <div class="form-group">
                                {!! Html::decode(Form::label('email', trans('strings.user.label_email'))) !!}
                                <div class="form-control">{{ $model->email }}</div>
                            </div>

                            <!-- Role Field -->
                            <div class="form-group">
                                {!! Html::decode(Form::label('role_id', trans('strings.user.label_role'))) !!}
                                <div class="form-control">{{ !empty($model->role) ? $model->role->name : '' }}</div>
                            </div>

                            <!-- Birthday Field -->
                            <div class="form-group">
                                {!! Form::label('birthday', trans('strings.user.label_birthday')) !!}
                                <div class="form-control">{{ Helper::getDateFromFormat($model->birthday) }}</div>
                            </div>

                            <!-- Gender Field -->
                            <div class="form-group">
                                {!! Form::label('gender', trans('strings.user.label_gender')) !!}
                                <div class="form-control">{{ (array_key_exists($model->gender, App\User::genders())) ? App\User::genders($model->gender) : '' }}</div>
                            </div>

                            <!-- Address Field -->
                            <div class="form-group">
                                {!! Form::label('address', trans('strings.user.label_address')) !!}
                                <div class="form-control">{{ nl2br($model->address) }}</div>
                            </div>

                            <!-- Phone Field -->
                            <div class="form-group">
                                {!! Form::label('phone', trans('strings.user.label_phone')) !!}
                                <div class="form-control">{{ $model->phone }}</div>
                            </div>

                            <!-- Description Field -->
                            <div class="form-group">
                                {!! Form::label('description', trans('strings.user.label_description')) !!}
                                <div class="form-control">{{ nl2br($model->description) }}</div>
                            </div>

                            <!-- Timezone Field -->
                            <div class="form-group">
                                {!! Form::label('timezone', trans('strings.user.label_timezone')) !!}
                                <div class="form-control">{{ (array_key_exists($model->timezone, App\User::timezones())) ? App\User::timezones($model->timezone) : '' }}</div>
                            </div>

                            <!-- Submit Field -->
                            <div class="form-group">
                                {!! Html::link(Admin::route('users.edit', [$model->id]), trans('strings.edit'), ['class' => 'btn btn-primary']) !!}
                                {!! Html::link(Admin::route('users.index'), trans('strings.cancel'), ['class' => 'btn btn-default']) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection