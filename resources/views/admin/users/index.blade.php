@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Manage Users</h2>
                <ul class="nav navbar-right panel_toolbox">
                    @if(Helper::checkUserPermission('admin.users.destroy'))
                        <li>
                            <a id="btn-sel-del" style="display:none;"
                               data-url="{{ Admin::route('contentManager.user.index') }}/" href="#"
                               class="btn-toolbox danger">
                                <i class="fa fa-trash"></i> Delete Selected Users</a>
                        </li>
                    @endif
                    @if(Helper::checkUserPermission('admin.users.create'))
                        <li>
                            <a href="{{ Admin::route('users.create') }}" class="btn-toolbox success">
                                <i class="fa fa-plus"></i> Create User</a>
                        </li>
                    @endif
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <input type="hidden" id="data-token" value="{{ csrf_token()}}">
                <input type="hidden" id="data-link-item-actions" value="{{ route('admin.users.itemActions') }}">

                @include('admin.users.partials.quick_search')
            </div>

            <div class="x_content">
                @include('admin.users.partials.table')
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    @include('ContentManager::partials.scriptdelete')
@endpush