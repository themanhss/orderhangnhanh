{{--<form method="POST" action="{{ ($model != "") ? Admin::route('users.update',['user'=>$model->id]) : Admin::route('users.store') }}">--}}
    <div class="col-md-4">
        @include('ContentManager::partials.imageUpload',['dataID'=>'userPhoto','dataValue'=>($model != "" ) ? $model->photo : old('photo'),'dataName'=>'photo'])
    </div>
    <div class="col-md-8">
        {{--{{ csrf_field() }}--}}
        {{--@if($model != "")
            --}}{{-- For update --}}{{--
            <input name="_method" type="hidden" value="PUT">
        @endif--}}

        <!-- Name Field -->
        <div class="form-group">
            {!! Html::decode(Form::label('name', trans('strings.user.label_name') . '<span class="required">*</span>')) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <!-- Email Field -->
        <div class="form-group">
            {!! Html::decode(Form::label('email', trans('strings.user.label_email') . '<span class="required">*</span>')) !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Role Field -->
        @if (empty($model) || $model->id != Auth::guard('admin')->user()->id)
        <div class="form-group">
            {!! Html::decode(Form::label('role_id', trans('strings.user.label_role') . '<span class="required">*</span>')) !!}
            {!! Form::select('role_id', $roles, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
        </div>
        @endif

        <!-- Birthday Field -->
        @php($fieldName = 'birthday')
        <div class="form-group">
            {!! Form::label($fieldName, trans('strings.user.label_birthday')) !!}
            {{--{!! Form::date('birthday', null, ['class' => 'form-control']) !!}--}}
            {{--<div class="input-group date" data-provide="datepicker">--}}
            <div class="input-group">
                @include('layouts.fields.date', [
                    'model' => (!empty($model)) ? $model : new \App\User(),
                    'name' => $fieldName,
                    'info' => ['name' => $fieldName],
                    'options' => [
                        'data-max-date' => Helper::getDateFromFormat(\Carbon\Carbon::now()),
                        'autocomplete' => 'off',
                    ]
                ])
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>

        <!-- Gender Field -->
        <div class="form-group">
            {!! Form::label('gender', trans('strings.user.label_gender')) !!}
            {!! Form::select('gender', $genders, null, ['class' => 'form-control select2']) !!}
        </div>

        <!-- Address Field -->
        <div class="form-group">
            {!! Form::label('address', trans('strings.user.label_address')) !!}
            {!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => 2]) !!}
        </div>

        <!-- Phone Field -->
        <div class="form-group">
            {!! Form::label('phone', trans('strings.user.label_phone')) !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Description Field -->
        <div class="form-group">
            {!! Form::label('description', trans('strings.user.label_description')) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 2]) !!}
        </div>

        <!-- Timezone Field -->
        @php($timezone = (!empty($model) && !empty($model->timezone)) ? $model->timezone : config('app.timezone'))
        <div class="form-group">
            {!! Form::label('timezone', trans('strings.user.label_timezone')) !!}
            {!! Form::select('timezone', App\User::timezones(), $timezone,
                ['class' => 'form-control select2']) !!}
        </div>

        @if ($model == "" || (($model != "" && !empty(Auth::guard('admin')->user())) && (Auth::guard('admin')->user()->id != $model->id)))
            <div class="form-group">
                {!! Form::label('is_admin', trans('strings.user.label_is_admin')) !!}
                <input type="hidden" name="is_admin" value="0" />
                {!! Form::checkbox('is_admin', 1, null, ['class' => 'js-switch']) !!}
            </div>
        @endif

        <!-- Submit Field -->
        <div class="form-group">
            {!! Form::submit(trans('strings.save'), ['class' => 'btn btn-primary']) !!}
            {!! Html::link(Admin::route('users.index'), trans('strings.cancel'), ['class' => 'btn btn-default']) !!}
        </div>
    </div>
{{--</form>--}}