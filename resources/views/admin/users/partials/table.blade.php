<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr>
        <th><input id="checkAll" type="checkbox" class="flat"></th>
        <th></th>
        <th>@lang('strings.user.label_name')</th>
        <th>@lang('strings.user.label_email')</th>
        <th>@lang('strings.user.label_role')</th>
        <th>@lang('strings.user.label_created_at')</th>
        <th>@lang('strings.user.label_last_login')</th>
        <th>@lang('strings.user.label_status')</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($model as $data)
        <tr id="tr-{{ $data->id }}">
            <td>
                <input type="checkbox" class="flat" name="checkbox" data-role="checkbox" value="{{$data->id}}"/>
                <input type="hidden" id="idPost" value="{{ $data->id }}">
            </td>
            <td><img src="{{ $data->photo }}" style="width: 100px;" /></td>
            <td>
                <div class="btn-edit-delete">
                    {{$data->name}}

                    @if ($data->is_admin)
                        <br>
                        <span class="label label-success">@lang('strings.admin')</span>
                    @endif
                </div>
            </td>
            <td><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
            <td>
                @if (!empty($data->role))
                    {{ $data->role->name }}
                @endif
            </td>
            <td>{!! Helper::getDateTimeFromFormat($data->created_at) !!}</td>
            <td>{!! Helper::getDateTimeFromFormat($data->last_login) !!}</td>
            <td>
                {{-- Active status --}}
                <div class="@if ($data->active) text-success @else text-danger @endif">
                    <span class="glyphicon @if ($data->active) glyphicon-record @else glyphicon-ban-circle @endif" aria-hidden="true"></span>
                    &nbsp;{!! \App\User::active_statuses((int)$data->active) !!}
                </div>
                {{-- Verification status --}}
                {{--<div class="@if ($data->is_verified) text-primary @else text-secondary @endif">
                    <span class="glyphicon @if ($data->is_verified) glyphicon-ok @else glyphicon-question-sign @endif" aria-hidden="true"></span>
                    &nbsp;{!! \App\User::verify_statuses((int)$data->is_verified) !!}
                </div>--}}
            </td>
            <td>
                @if(Helper::checkUserPermission('admin.users.show'))
                    <a href="{{ Admin::route('users.show', ['user'=>$data->id]) }}"
                       class="text-info">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                @endif
                @if(Helper::checkUserPermission('admin.users.edit'))
                    &nbsp;
                    <a href="{{ Admin::route('users.edit', ['user'=>$data->id]) }}"
                       class="text-success">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                @endif
                @if(Helper::checkUserPermission('admin.users.destroy'))
                    &nbsp;
                    <a href="#" data-role="delete-post" data-idpost="{{ $data->id }}"
                       data-url="{{ Admin::route('contentManager.user.destroy',['tag'=>'']) }}/"
                       data-title="Delete this user"
                       class="text-danger">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                @endif

                @if(Helper::checkUserPermission('admin.users.itemActions'))
                    &nbsp;
                    {{-- Ban/un-ban --}}
                    @php
                        $statusVal = ($data->active == \App\User::IS_YES) ? 0 : 1;
                        $statusText = ($data->active == \App\User::IS_YES) ? 'ban' : 'unban';
                        $statusIcon = ($data->active == \App\User::IS_YES) ? 'fa-ban' : 'fa-play-circle';
                    @endphp

                    <a href="javascript:void(0)" class="item_actions" data-link="data-link-item-actions" data-val="{{ $statusVal }}"
                       data-key="status" data-text="{{ $statusText }}" data-id="{!! $data->id !!}" data-title="{!! $data->name !!}" title="{{ $statusText }}">
                        <i class="fa {{ $statusIcon }}" aria-hidden="true"></i>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $model->links() }}