@push('style-top')
    <link id="themecss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />
@endpush

@push('style')
    <style>
        .sui-treeview {
            padding: 0;
        }
    </style>
@endpush

<!-- Name Field -->
<div class="form-group">
    {!! Html::decode(Form::label('name', trans('strings.role.label_name') . '<span class="required">*</span>')) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', trans('strings.role.label_description')) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3, 'style' => 'resize: vertical;']) !!}
</div>

<!-- Permission Field -->
<div class="form-group">
    {!! Html::decode(Form::label('permission', trans('strings.role.label_permission') . '<span class="required">*</span>')) !!}
    {{--{!! Form::textarea('permission', null, ['class' => 'form-control']) !!}--}}
    <!-- Permission List -->
    @php
    $permissionsList = config("permission");
    /*$permissionAssigned = isset($role->permission) ? $role->permission : null;
    $permissionAssigned = ($permissionAssigned != null) ? json_decode($permissionAssigned) : [];

    if ($permissionAssigned != null) {
        $permissionAssigned = get_object_vars($permissionAssigned);
    }*/
    $permissionAssigned = (!empty($role) && $role->permission !== null) ? $role->permission : [];

    $assignAll = [];
    @endphp

    <div class="row">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="all" id="full-permissions">
                <span class="">@lang('strings.role.label_full_permissions')</span>
            </label>
        </div>
    </div>

    <div class="row">
        <ul id="treeview">
            @foreach($permissionsList as $key => $pms)
                @php
                $assignAll = '';
                $total = count($pms['actions']);
                $countTrue = 0;
                $assignChild = [];
                foreach($pms['actions'] as $elem){
                    $assigned = '';
                    $fieldData = explode('@', $elem['action']);
                    $parentKey = Helper::recursive_array_search($elem['action'], $permissionAssigned);

                    if ($parentKey !== false) {
                        $assigned = (array_key_exists($parentKey, $permissionAssigned) ? 'checked' : '');
                    }

                    if('checked' == $assigned){
                        $countTrue++;
                    }

                    $assignChild[] = [
                        'name' => $elem['name'],
                        'assigned' => $assigned,
                        'action' => $elem["action"]
                    ];
                }

                if($countTrue == $total){
                    $assignAll = 'checked';
                }
                @endphp
                <input type="hidden" class="ck_parent_{!! $key !!}_total" value="{!! $total !!}">
                <input type="hidden" class="ck_parent_{!! $key !!}_count_true" value="{!! $countTrue !!}">
                <li id='ck_parent_{!! $key !!}' data-icon-cls="fa fa-folder" data-expanded="true">
                    <input class="ck_parent_{!! $key !!}" type='checkbox' {!! $assignAll !!}>
                    {!! Helper::trans($pms['name'], [], 'permission.' . $key) !!}
                    <ul>
                        @foreach($assignChild as $_key => $elem)
                            <li>
                                <input class="ck_children_{!! $key !!} ck_children_{!! $key . '_' . $_key !!}"
                                       id='ck_children_{!! $_key !!}' name='permission[{!! $fieldData[0] !!}][]'
                                       type='checkbox' value='{!! $elem["action"] !!}' {!! $elem['assigned'] !!}>
                                {!! Helper::trans($elem['name'], [], 'permission.' . $key) !!}
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    {!! Form::submit(trans('strings.save'), ['class' => 'btn btn-primary clear-border-radius text-uppercase']) !!}
    {{--<a href="{!! route('admin.roles.index') !!}" class="btn btn-dark clear-border-radius text-uppercase">@lang('strings.cancel')</a>--}}
    <button type="button" class="btn btn-dark clear-border-radius text-uppercase"
            onclick="window.history.back();">@lang('strings.cancel')</button>
</div>

@push('scripts')
    <script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

    <script>
        $(document).ready(function () {
            var treeview = $("#treeview");

            treeview.shieldTreeView({
                /*events: {
                    select: function (e) {
                        $('span.sui-treeview-item-text').css({
                            'background-color': '#fff',
                            'color': '#6d6d6d'
                        });
                    },
                },*/
            });

            /*$('span.sui-treeview-item-text').on('mouseover', function () {
                $('span.sui-treeview-item-text').css({
                    'background-color': '#fff',
                    'color': '#6d6d6d'
                });
            });*/

            @foreach($permissionsList as $key => $pms)
            $('.ck_parent_{!! $key !!}').on('change', function () {
                var checked = $(this).is(":checked");

                if (!checked) {
                    $(".ck_children_{!! $key !!}").prop('checked', false);
                    $('.ck_parent_{!! $key !!}_count_true').val(0);
                } else {
                    $(".ck_children_{!! $key !!}").prop('checked', true);
                    $('.ck_parent_{!! $key !!}_count_true').val($('.ck_parent_{!! $key !!}_total').val());
                }
            });

            @foreach($pms['actions'] as $_key => $elem)
            $(".ck_children_{!! $key . '_' . $_key !!}").on('change', function () {
                var checked = $(this).is(":checked");
                var name = $(this).attr('name');
                var value = $(this).val();

                if (!checked) {
                    $('.ck_parent_{!! $key !!}_count_true').val(parseInt($('.ck_parent_{!! $key !!}_count_true').val()) - 1);
                    $('.ck_parent_{!! $key !!}').prop('checked', false);
                } else {
                    $('.ck_parent_{!! $key !!}_count_true').val(parseInt($('.ck_parent_{!! $key !!}_count_true').val()) + 1);

                    if ($('.ck_parent_{!! $key !!}_count_true').val() == $('.ck_parent_{!! $key !!}_total').val()) {
                        $('.ck_parent_{!! $key !!}').prop('checked', true);
                    }
                }

                // Fix issue when duplicate input with treeview
                treeview.find('[name="' + name + '"][value="' + value + '"]').prop('checked', checked);
            });
            @endforeach
            @endforeach

            //Process permission list
            $("#administrator-role").change(function () {
                if (this.checked) {
                    $('#checkBoxChild-administrator, #checkBoxParent-administrator').prop('checked', true);
                } else {
                    $('#checkBoxChild-administrator, #checkBoxParent-administrator').prop('checked', false);
                }
            });

            //Expand or Collapse panel
            /*$('.btn-info').click(function () {
                var currentID = $(this).attr('data-target');

                $('.btn-info').each(function (index, obj) {
                    var ID = $(this).attr('data-target');

                    if (currentID != ID) {
                        $('div' + ID).removeClass('in');
                        $('div' + ID).removeAttr('aria-expanded');
                        $(this).removeAttr('aria-expanded');
                    }
                });
            });*/

            $('#full-permissions').on('change', function () {
                var input = $(this);
                // var checked = input.prop('checked');
                var treeviewCheckbox = treeview.next('.sui-treeview-list').find('input[type="checkbox"]');
                treeviewCheckbox.prop('checked', input.prop('checked'));
            });
        });
    </script>
@endpush
