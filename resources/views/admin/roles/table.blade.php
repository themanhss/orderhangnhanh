<table class="table table-responsive" id="roles-table">
    <thead>
        <tr>
            <th>@lang('strings.role.label_name')</th>
            <th>@lang('strings.role.label_description')</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($roles as $role)
        <tr>
            <td>{!! $role->name !!}</td>
            <td>{!! $role->description !!}</td>
            <td>
                @if(Helper::checkUserPermission('admin.roles.edit'))
                    <a href="{!! route('admin.roles.edit', [$role->id]) !!}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                @endif

                @if (!in_array($role->id, $role->getHiddenRoles()))
                    @if(Helper::checkUserPermission('admin.roles.destroy'))
                        {!! Form::open(['route' => ['admin.roles.destroy', $role->id], 'method' => 'delete', 'class' => 'inline-block']) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>',
                                ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        {!! Form::close() !!}
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@if(!empty($roles))
    <div class="pagination-area text-center">
        {{ $roles->links() }}
    </div>
@endif