<li><a href="{{ Admin::route('contentManager.dashboard') }}"><i class="fa fa-home"></i> Dashboard </a></li>
<li {{ Admin::requestIs('contentManager/post*') ? ' class=active' : '' }}><a><i class="fa fa-newspaper-o"></i> Content Manager <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
        <li><a href="{{ Admin::route('contentManager.page.index') }}">Pages</a></li>
        <li><a href="{{ Admin::route('contentManager.post.index') }}">Posts</a></li>
        <li><a href="{{ Admin::route('contentManager.comment') }}">Comments </a></li>
        <li><a href="{{ Admin::route('contentManager.category.index') }}">Categories</a></li>
        <li><a href="{{ Admin::route('contentManager.tag.index') }}">Tags</a></li>
    </ul>
</li>
<li><a href="{{ Admin::route('contentManager.menu.index') }}"><i class="fa fa-sitemap"></i> Menu Manager </a></li>
<li><a href="{{ Admin::route('contentManager.media') }}"><i class="fa fa-file-image-o"></i> Media Manager </a></li>
<li><a><i class="fa fa-desktop"></i> Theme Manager <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
        <li><a href="{{ Admin::route('contentManager.theme') }}">Manage Theme</a></li>
        <li><a href="{{ Admin::route('contentManager.theme.view',['id'=>Theme::getID()]) }}">Edit Theme Active</a></li>
    </ul>
</li>
<li style="display: none;"><a href="{{ Admin::route('contentManager.widget') }}"><i class="fa fa-clone"></i> Widget Manager </a></li>
@if(count(Admin::listModule()) > 0)
    <li style="display: none;"><a><i class="fa fa-edit"></i> Modules Manager <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            @foreach(Admin::listModule() as $module => $routeIndex)
                <li><a href="{{ Admin::route($routeIndex) }}">{{ ucwords(str_replace("_"," ",snake_case($module))) }}</a></li>
            @endforeach
        </ul>
    </li>
@endif
<li style="display: none;"><a href="{{ Admin::route('contentManager.setting') }}"><i class="fa fa-gear"></i> Setting</a></li>
<li><a><i class="fa fa-gear"></i> Settings <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
        @if(Helper::checkUserPermission('admin.users.index'))
            <li><a href="{{ Admin::route('users.index') }}"><i class="fa fa-users"></i> Users Manager </a></li>
        @endif
        @if(Helper::checkUserPermission('admin.roles.index'))
            <li class="{{ Request::is('roles*') ? 'active' : '' }}">
                <a href="{!! route('admin.roles.index') !!}"><i class="fa fa-user-secret"></i><span>Roles Manager</span></a>
            </li>
        @endif
        @if(Helper::checkUserPermission('admin.countries.index'))
            <li class="{{ Request::is('countries*') ? 'active' : '' }}">
                <a href="{!! route('admin.countries.index') !!}"><i class="fa fa-globe"></i><span>Countries Manager</span></a>
            </li>
        @endif
        @if(Helper::checkUserPermission('admin.contacts.index'))
            <li class="{{ Request::is('contacts*') ? 'active' : '' }}">
                <a href="{!! route('admin.contacts.index') !!}"><i class="fa fa-th-list"></i><span>Contacts Manager</span></a>
            </li>
        @endif
        @if(Helper::checkUserPermission('admin.banners.index'))
            <li class="{{ Request::is('banners*') ? 'active' : '' }}">
                <a href="{!! route('admin.banners.index') !!}"><i class="fa fa-th-list"></i><span>Banners Manager</span></a>
            </li>
        @endif
    </ul>
</li>
