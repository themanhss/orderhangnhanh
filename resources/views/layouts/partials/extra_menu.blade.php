<ul class="nav navbar-nav navbar-right">
    <li class="">
        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="{{ Auth::guard('admin')->user()->photo }}" alt="">{{ Auth::guard('admin')->user()->name }}
            <span class=" fa fa-angle-down"></span>
        </a>
        <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="{{ Admin::route('users.show',['user'=>Auth::guard('admin')->user()->id]) }}"> Profile</a></li>
            <li><a href="{{ Admin::route('contentManager.setting') }}"><i class="fa fa-gear pull-right"></i> Setting</a></li>
            <li><a href="{{ Admin::route('password.changePasswordForm') }}"><i class="fa fa-lock pull-right"></i> Change password</a></li>
            <li><a href="{{ Admin::route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
        </ul>
    </li>

    <li class="dropdown">
        @include('layouts.partials.switch_lang')
    </li>

    <li role="presentation" class="dropdown" style="display: none;">
        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>
            <span class="badge bg-green">6</span>
        </a>

        @include('layouts.partials.msg_list')
    </li>
</ul>