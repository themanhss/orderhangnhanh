@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default mail-success-panel">
                    <div class="panel-heading text-center">@lang('strings.user.changed_email_successfully')</div>
                </div>
            </div>
        </div>
    </div>
@endsection
