<?php

return [
    'user' => [
        'subject_create_admin' => 'Product Manager new Admin account created',
        'subject_account_verification' => 'Product Manager account verification',
        'subject_confirm_change_email' => 'Confirm request to change email address',
    ],
];