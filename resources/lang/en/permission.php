<?php

return [
    'user' => [
        'User Management' => 'User Management',
        'List' => 'List',
        'View' => 'View',
        'Add New' => 'Add',
        'Edit' => 'Edit',
        'Delete' => 'Delete',
        'Ban' => 'Ban',
    ],
];