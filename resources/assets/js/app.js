"use strict";

(function ($) {
    // Date picker
    $('.datepicker').each(function () {
        var txtDate = $(this);

        // Date picker
        var datepickerOptions = {};

        // Min date
        var attrMinDate = txtDate.data('min-date');

        if (attrMinDate) {
            datepickerOptions['startDate'] = attrMinDate;
        }

        // Max date
        var attrMaxDate = txtDate.data('max-date');

        if (attrMaxDate) {
            datepickerOptions['endDate'] = attrMaxDate;
        }

        // Register date picker
        txtDate.datepicker(datepickerOptions)
        /**
         * Listen for the change even on the input
         * @link https://stackoverflow.com/a/22507814/10174865
         */
            .on('changeDate', function (e) {
                // Hide picker
                $(this).datepicker('hide');

                var input = $(this);
                var field = input;
                var container = input.parent();
                var name = input.attr('name');
                // Split name to array to detect is display field or not
                var arrName = name.split('_');
                var isDisplayField = (arrName[arrName.length - 1] === 'display');

                if (isDisplayField) {
                    var fieldInfo = input.data('info');
                    field = container.find('[name="' + fieldInfo.name + '"]');
                }

                var date = $(this).datepicker('getDate');
                var strDate = moment(date).format('YYYY-MM-DD');

                if (isDisplayField && field.length > 0) {
                    field.val(strDate);
                }
            });
    });

    // Select2
    $('.select2').select2();
    
    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();
})(jQuery);