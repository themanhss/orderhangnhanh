<?php
return [
    /*'dashboard' => [
        'name' => 'Dashboard',
        'actions' => [
            [
                'name' => 'Dashboard',
                'action' => 'default@index'
            ],
        ]
    ],*/
    'role' => [
        'name' => 'Roles and Permissions',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'role@index'
            ],
            /*[
                'name' => 'View',
                'action' => 'role@show'
            ],*/
            [
                'name' => 'Add New',
                'action' => 'role@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'role@edit'
            ],
            [
                'name' => 'Delete',
                'action' => 'role@destroy'
            ],
        ]
    ],
    'user' => [
        'name' => 'Users',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'user@index'
            ],
            [
                'name' => 'View',
                'action' => 'user@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'user@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'user@edit'
            ],
            [
                'name' => 'Delete',
                'action' => 'user@destroy'
            ],
            [
                'name' => 'Ban',
                'action' => 'user@itemactions'
            ],
        ]
    ],
    'post' => [
        'name' => 'Content',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'post@index'
            ],
            [
                'name' => 'View',
                'action' => 'post@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'post@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'post@edit'
            ],
            [
                'name' => 'Delete',
                'action' => 'post@destroy'
            ],
            [
                'name' => 'Delete Multi',
                'action' => 'post@bulkactions'
            ],
        ]
    ],
    'banner' => [
        'name' => 'Banners',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'banner@index'
            ],
            [
                'name' => 'Add New',
                'action' => 'banner@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'banner@edit'
            ],
            [
                'name' => 'Delete',
                'action' => 'banner@destroy'
            ],
            [
                'name' => 'Change Order',
                'action' => 'banner@changeorder'
            ],
        ]
    ],
    'contact' => [
        'name' => 'Contacts',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'contact@index'
            ],
            [
                'name' => 'View',
                'action' => 'contact@show'
            ]
        ]
    ],
    'country' => [
        'name' => 'Countries',
        'actions' => [
            [
                'name' => 'List',
                'action' => 'country@index'
            ],
            [
                'name' => 'View',
                'action' => 'country@show'
            ],
            [
                'name' => 'Add New',
                'action' => 'country@create'
            ],
            [
                'name' => 'Edit',
                'action' => 'country@edit'
            ],
            [
                'name' => 'Delete',
                'action' => 'country@destroy'
            ],
        ]
    ],
];